// HENIKA -> [ math-three ] .r1

import * as THREE from './three.module.min.js';
import * as BufferGeometryUtils from './BufferGeometryUtils.js';
import { FontLoader } from './FontLoader.js';
import Stats from './stats.module.js';
const mainRenderer = new THREE.WebGLRenderer({ antialias: true, powerPreference: "high-performance" });
mainRenderer.outputColorSpace = THREE.SRGBColorSpace;
mainRenderer.setPixelRatio(window.devicePixelRatio);
document.body.appendChild(mainRenderer.domElement);
const mainScene = new THREE.Scene();
mainScene.background = new THREE.Color(0x64FFDA);


const mainCamera = new THREE.OrthographicCamera(document.body.clientWidth / -2, window.innerWidth / 2, document.body.clientHeight / 2, document.body.clientHeight / -2, 0.01, 6000);

const ambientLight = new THREE.AmbientLight(0xFFFFFF, 1);
const directionalLight = new THREE.DirectionalLight(0xFFFFFF, 1);
directionalLight.position.set(-1, 1, 1);
mainScene.add(ambientLight, directionalLight);
const button = [];
let oldWindow;
onWindowResize();
function onWindowResize() {
	if (document.body.clientHeight / document.body.clientWidth < 1.6) {
		mainScene.scale.set(document.body.clientHeight / 520, document.body.clientHeight / 520, document.body.clientHeight / 520);
	
				
	} else {
		
		mainScene.scale.set(document.body.clientWidth / 360, document.body.clientWidth / 360, document.body.clientWidth / 360);
	
		
		
	}
	
	//	alert(mainScene.scale.x)
	
	mainRenderer.setSize(document.body.clientWidth, document.body.clientHeight);
	
	
	mainCamera.left = -document.body.clientWidth / 2;
	mainCamera.right = document.body.clientWidth / 2;
	mainCamera.top = document.body.clientHeight / 2;
	mainCamera.bottom = -document.body.clientHeight / 2;
	
	
	
	mainCamera.updateProjectionMatrix();
	oldWindow = document.body.clientWidth / document.body.clientHeight;
	
}

let Bagel;
new FontLoader().load('./fonts/Bagel.json', function(font) {
	Bagel = font;
	createGraphics();
});

let onHold = false;


const cell = [];
const cellsContainer = new THREE.Object3D();
const transparentMaterial = new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 });
const tempGeometry = [];

const cellBorderColor = [
	0x78909C, 0xF06292, 0x29B6F6, 0x66BB6A, 0xFFB74D, 0x9575CD, 0xE57373, 0x7986CB, 0x4DB6AC, 0xC0CA33
]
const cellNumColor = [
	0x607D8B, 0xEC407A, 0x0288D1, 0x388E3C, 0xF57C00, 0x7E57C2, 0xEF5350, 0x5C6BC0, 0x00897B, 0x8D8D20, 0xAB47BC
]
const cellBodyColor = [
	0xFF5252, 0xFCE4EC, 0xE1F5FE, 0xE8F5E9, 0xFFF8E1, 0xEDE7F6, 0xFFEBEE, 0xE8EAF6, 0xE0F2F1, 0xF9FBE7, 0xF3E5F5, 0xFBE9E7
]
let isCellsFieldActive = true;
let cellsToCheck = [];
const blimMaterial = [];
for (let i = 0; i < 5; i++) {
	blimMaterial[i] = new THREE.MeshBasicMaterial({ color: 0xFFFFFF });
	
	//gsap.to(blimMaterial[i].color, { duration: 0.3 + Math.random() * 0.3, setHex: 0xFFFFFF, ease: "none", repeat: -1 });
}
let chosenCell;



const moveCounter = new THREE.Object3D();
let moveCount = 3;



const cellObject = [];
const cellImg = [];
let onPlay = true;
let onSwipe = true;
let farmerCell = 24;
let cellColor = [
	new THREE.Color(0xFFFFFF),
	new THREE.Color(0xFFCCBC),
	new THREE.Color(0x76FF03),
	new THREE.Color(0xFFEA00),
	new THREE.Color(0xE1BEE7),
	new THREE.Color(0x18FFFF)
];
let cellList = [];
const sparkleMaterial = [
	new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0.7 }),
  new THREE.MeshBasicMaterial({ color: 0xFF9E80, transparent: true, opacity: 0.7 }),
  new THREE.MeshBasicMaterial({ color: 0xAEEA00, transparent: true, opacity: 0.7 }),
  new THREE.MeshBasicMaterial({ color: 0xFFEA00, transparent: true, opacity: 0.7 }),
  new THREE.MeshBasicMaterial({ color: 0xEA80FC, transparent: true, opacity: 0.7 }),
  new THREE.MeshBasicMaterial({ color: 0x18FFFF, transparent: true, opacity: 0.7 })
];
let activePowerUp = -1;

function createGraphics() {
	
	const sphere = new THREE.Mesh(new THREE.SphereGeometry(10, 18, 8), new THREE.MeshLambertMaterial({ color: 0xF8BBD0 }));

  sphere.position.z = - 10
  
 // mainScene.add(sphere)
	
	const sparkleGeometry = [new THREE.CircleGeometry(0.1, 16), new THREE.RingGeometry(0.07, 0.1, 14, 1)];
  
	
	cellObject[0] = new THREE.Object3D();
	cellObject[0].container = new THREE.Object3D();
	cellObject[0].body = new THREE.Object3D();
	tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.1, 18, 8, 0, Math.PI * 2, Math.PI * 0.25, Math.PI * 0.4);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.05, 0.015, 10, 3, Math.PI * 0.2);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(-0.2, 0, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.11, -0.18, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(-0.8);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length - 1].rotateX(-1.6);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.015, 10, 5, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.25, 0, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.11, -0.18, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(-0.8);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length - 1].rotateX(-1.6);
	cellObject[0].head = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0xF8BBD0 }));
	clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.1, 18, 8, 0, Math.PI * 2, Math.PI * 0.65, Math.PI * 0.35);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.1003, 16, 1, 0, Math.PI * 1.1, Math.PI * 0.21, Math.PI * 0.06);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.55);
  tempGeometry[tempGeometry.length - 1].rotateZ(-2.3);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.04);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 1.08);
  cellObject[0].pants = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x42A5F5 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.03, 0.009, 0.06, 16, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(-0.04, -0.09, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  cellObject[0].legs = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x42A5F5 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.018, 12, 6, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 1.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.012);
  tempGeometry[tempGeometry.length - 1].rotateY(-0.3);
  tempGeometry[tempGeometry.length - 1].translate(-0.04, -0.12, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(0.04, 0, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(0.6);
  tempGeometry[tempGeometry.length - 1].translate(0.04, 0, 0);
  cellObject[0].boots = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x8D6E63 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.005, 0.005, 0.1, 8, 1, true);
  tempGeometry[tempGeometry.length - 1].rotateX(0.2);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.25);
  tempGeometry[tempGeometry.length - 1].translate(0.103, -0.06, 0.042);
  cellObject[0].handle = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x8D6E63 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.005, 0.005, 0.02, 8, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.06, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(0.2);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.25);
  tempGeometry[tempGeometry.length - 1].translate(0.103, -0.06, 0.042);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.005, 0.005, 0.05, 6, 1, true);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI / 6);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.065, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(0.4);
  tempGeometry[tempGeometry.length - 1].rotateX(0.2);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.25);
  tempGeometry[tempGeometry.length - 1].translate(0.103, -0.06, 0.042);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.005, 0.005, 6, 4, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0.025, 0.07, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(0.4);
  tempGeometry[tempGeometry.length - 1].rotateX(0.2);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.25);
  tempGeometry[tempGeometry.length - 1].translate(0.103, -0.06, 0.042);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.005, 0.005, 6, 4, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(-0.025, 0.07, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(0.4);
  tempGeometry[tempGeometry.length - 1].rotateX(0.2);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.25);
  tempGeometry[tempGeometry.length - 1].translate(0.103, -0.06, 0.042);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.005, 6, 6, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 12, 1);
  tempGeometry[tempGeometry.length - 1].translate(-0.03, 0.07, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(0.4);
  tempGeometry[tempGeometry.length - 1].rotateX(0.2);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.25);
  tempGeometry[tempGeometry.length - 1].translate(0.103, -0.06, 0.042);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.005, 6, 6, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 12, 1);
  tempGeometry[tempGeometry.length - 1].translate(0.03, 0.07, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(0.4);
  tempGeometry[tempGeometry.length - 1].rotateX(0.2);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.25);
  tempGeometry[tempGeometry.length - 1].translate(0.103, -0.06, 0.042);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.005, 8, 6, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 12, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.07, 0);
  tempGeometry[tempGeometry.length - 1].rotateX(0.2);
  tempGeometry[tempGeometry.length - 1].rotateZ(-0.25);
  tempGeometry[tempGeometry.length - 1].translate(0.103, -0.06, 0.042);
  cellObject[0].pitchfork = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0xE0E0E0 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.083, 16, 10, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.055, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(0.1);
  tempGeometry[tempGeometry.length - 1].rotateX(-0.1);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.14, 0.14, 0.005, 32, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.055, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(0.1);
  tempGeometry[tempGeometry.length - 1].rotateX(-0.1);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.1004, 10, 1, 0, Math.PI * 2, 0, 0.07);
  tempGeometry[tempGeometry.length - 1].rotateX(2.15);
  tempGeometry[tempGeometry.length - 1].rotateY(0.52);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-1.04);
  cellObject[0].hat = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0xFF9E80 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.01, 12, 12);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.103);
  tempGeometry[tempGeometry.length - 1].rotateY(0.25);
  tempGeometry[tempGeometry.length - 1].rotateX(-0.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-0.5);
  cellObject[0].eyes = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x424242 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.0102, 8, 1, 0, Math.PI * 2, 0, 0.3);
  tempGeometry[tempGeometry.length - 1].rotateX(0.8);
  tempGeometry[tempGeometry.length - 1].rotateZ(1.3);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, 0.103);
  tempGeometry[tempGeometry.length - 1].rotateY(0.25);
  tempGeometry[tempGeometry.length - 1].rotateX(-0.2);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.05, 0, 0);
  cellObject[0].highlights = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFFFFFF }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.RingGeometry(0.013, 0.02, 9, 1, Math.PI * 1.2, Math.PI * 0.6);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.0035, 4, Math.PI * 0.2, Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.0165 * Math.cos(Math.PI * 1.2), 0.0165 * Math.sin(Math.PI * 1.2), 0);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.0035, 4, Math.PI * 1.8, Math.PI);
  tempGeometry[tempGeometry.length - 1].translate(0.0165 * Math.cos(Math.PI * 1.8), 0.0165 * Math.sin(Math.PI * 1.8), 0);
  cellObject[0].smile = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x424242 }));
  clearTempGeometries();
  bendGeometry(cellObject[0].smile.geometry, "y", -15);
  cellObject[0].smile.geometry.translate(0, 0, 0.1005);
  cellObject[0].body.add(cellObject[0].pitchfork, cellObject[0].head, cellObject[0].hat, cellObject[0].eyes, cellObject[0].pants, cellObject[0].highlights, cellObject[0].smile, cellObject[0].handle);
	cellObject[0].add(cellObject[0].body, cellObject[0].legs, cellObject[0].boots);
  cellObject[0].scale.set(1.7, 1.7, 1.7);
  cellObject[0].rotation.x = 0.2;
  cellObject[0].position.y = 0.04;
  cellObject[0].container.add(cellObject[0]);


   
  cellObject[1] = new THREE.Object3D();
  cellObject[1].container = new THREE.Object3D();
  cellObject[1].body = new THREE.Mesh(new THREE.SphereGeometry(0.095, 30, 20), new THREE.MeshLambertMaterial({ color: 0xF06360 }));
  cellObject[1].body.geometry.scale(1, 0.9, 1);
  tempGeometry[tempGeometry.length] = new THREE.ConeGeometry(0.02, 0.08, 3, 7, true, Math.PI * 1.25, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.04, 0);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", -30);
  tempGeometry[tempGeometry.length - 1].scale(1.3, 1, 1.6);
  tempGeometry[tempGeometry.length - 1].rotateZ(-1.6);
  tempGeometry[tempGeometry.length - 1].translate(0.01, 0.06, 0);
  for (let i = 0; i < 4; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
    tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2.5);
  }
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.006, 0.003, 0.05, 5, 5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.025, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", -15);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.075, 0);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.015, 5);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.0855, 0);
  cellObject[1].leaves = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x43A047, side: THREE.DoubleSide }));
  clearTempGeometries();
  cellObject[1].add(cellObject[1].body, cellObject[1].leaves);
  cellObject[1].scale.set(1.3, 1.3, 1.3);
  cellObject[1].rotation.x = 0.2;
  cellObject[1].container.add(cellObject[1]);
  cellObject[1].container.rotation.set(-0.2, -0.2, -0.2);
  cellObject[2] = new THREE.Object3D();
  let path = new THREE.Path();
  path.absarc(0.04, 0, 0.045, Math.PI * 0.82, 0, true);
  path.absellipse(0.025, -0.005, 0.06, 0.09, Math.PI * 2, Math.PI * 1.38, true);
  cellObject[2].body = new THREE.Mesh(new THREE.LatheGeometry(path.getPoints(8), 28), new THREE.MeshLambertMaterial({ color: 0x4CAF50, side: THREE.BackSide }));
  cellObject[2].body.geometry.translate(0, 0.024, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.006, 0.003, 0.05, 5, 5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.025, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", -15);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.05, 0);
  cellObject[2].stalk = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x8D6E63 }));
  clearTempGeometries();
  cellObject[2].add(cellObject[2].body, cellObject[2].stalk);
  cellObject[2].container = new THREE.Object3D();
  cellObject[2].scale.set(1.45, 1.45, 1.45);
  cellObject[2].rotation.x = 0.2;
  cellObject[2].container.add(cellObject[2]);
  cellObject[2].container.rotation.set(-0.2, -0.2, -0.2);
  cellObject[3] = new THREE.Object3D();
  cellObject[3].container = new THREE.Object3D();
  path = null;
  path = new THREE.Path();
  path.absarc(0, 0, 0.05, Math.PI * 0.5, 0, true);
  path.absarc(0.07, 0, 0.02, Math.PI, Math.PI * 1.25);
  path.absarc(0.0134, -0.0566, 0.06, Math.PI * 2.25, Math.PI * 1.45, true);
  cellObject[3].body = new THREE.Mesh(new THREE.LatheGeometry(path.getPoints(8), 28), new THREE.MeshLambertMaterial({ color: 0xFFCA28, side: THREE.BackSide }));
  cellObject[3].body.geometry.translate(0, 0.035, 0);
  cellObject[3].body.geometry.scale(1, 0.9, 1);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.006, 0.003, 0.05, 5, 5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.025, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", -15);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(0.8, 0.8, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.071, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.008, 5, 2, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.5, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.073, 0);
  cellObject[3].stalk = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x8D6E63 }));
  clearTempGeometries();
  cellObject[3].add(cellObject[3].body, cellObject[3].stalk);
  cellObject[3].scale.set(1.6, 1.6, 1.6);
  cellObject[3].rotation.x = 0.2;
  cellObject[3].container.add(cellObject[3]);
  cellObject[3].container.rotation.set(-0.2, -0.2, -0.2);
  cellObject[4] = new THREE.Object3D();
  cellObject[4].container = new THREE.Object3D();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.06, 24, 18, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.8, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.06, 0);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", -25);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.06, 24, 9, 0, Math.PI * 2, Math.PI * 0.5, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.06, 0);
  cellObject[4].body = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0xBA68C8 }));
  clearTempGeometries();
  cellObject[4].body.geometry.rotateZ(-2.6);
  cellObject[4].body.geometry.translate(-0.045, 0.032, 0);
  tempGeometry[tempGeometry.length] = new THREE.ConeGeometry(0.02, 0.08, 3, 5, true, Math.PI * 1.25, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.04, 0);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", -30);
  tempGeometry[tempGeometry.length - 1].scale(1.3, 1, 2.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(-2.1);
  tempGeometry[tempGeometry.length - 1].translate(0.01, 0.06, 0);
  for (let i = 0; i < 4; i++) {
  	tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  	tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 2.5);
  }
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.006, 0.01, 0.05, 5, 5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.025, 0);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", -15);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.08, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.03, 5, 1, 0, Math.PI * 2, 0, 0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.064, 0);
  cellObject[4].leaves = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x43A047, side: THREE.DoubleSide }));
  clearTempGeometries();
  cellObject[4].leaves.geometry.scale(0.6, 0.6, 0.6);
  cellObject[4].leaves.geometry.rotateY(Math.PI);
  cellObject[4].leaves.geometry.rotateZ(-0.2);
  cellObject[4].leaves.geometry.translate(0.034, 0.0322, 0);
  cellObject[4].add(cellObject[4].body, cellObject[4].leaves);
  cellObject[4].scale.set(1.6, 1.6, 1.6);
  cellObject[4].rotation.x = 0.2;
  cellObject[4].position.set(-0.003, -0.007, 0);
  cellObject[4].container.add(cellObject[4]);
  cellObject[4].container.rotation.set(-0.2, -0.2, -0.2);
  

  cellObject[5] = new THREE.Object3D();
  cellObject[5].container = new THREE.Object3D();
  cellObject[5].hole = new THREE.Object3D();
  cellObject[5].holeBorder = new THREE.Mesh(new THREE.TorusGeometry(0.14, 0.012, 10, 24), new THREE.MeshLambertMaterial({ color: 0xA1887F }));
  cellObject[5].holeBorder.geometry.rotateX(Math.PI * 0.5);
  cellObject[5].deep = new THREE.Mesh(new THREE.CircleGeometry(0.15, 6), new THREE.MeshLambertMaterial({ color: 0x6D4C41 }));
  cellObject[5].deep.geometry.rotateX(-Math.PI * 0.5);
  cellObject[5].hole.add(cellObject[5].holeBorder, cellObject[5].deep);
  cellObject[5].mole = new THREE.Object3D();
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.11, 0.11, 0.18, 18, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.08, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.11, 18, 9, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.17, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI / 18);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.045, 18, 6, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 2, 1);
  tempGeometry[tempGeometry.length - 1].rotateX(1.2);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.13, 0.08);
  tempGeometry[tempGeometry.length] = new THREE.TorusGeometry(0.055, 0.017, 12, 4, Math.PI * 0.25);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].scale(1.5, 1.5, 1.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.07, 0.14, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.8);
  cellObject[5].moleBody = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x607D8B }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.016, 12, 8);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.18, 0.103);
  tempGeometry[tempGeometry.length - 1].rotateY(0.35);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(-0.7);
  cellObject[5].eyes = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0x424242 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.0162, 8, 1, 0, Math.PI * 2, 0, 0.3);
  tempGeometry[tempGeometry.length - 1].rotateX(0.7);
  tempGeometry[tempGeometry.length - 1].rotateZ(0.9);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.18, 0.103);
  tempGeometry[tempGeometry.length - 1].rotateY(0.35);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-0.071, 0, 0);
  cellObject[5].highlights = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFFFFFF }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.014, 12, 6, 0, Math.PI * 2, 0, 2);
  tempGeometry[tempGeometry.length - 1].rotateX(1);
  tempGeometry[tempGeometry.length - 1].translate(0, 0.16, 0.16);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(0.017, 12, 5, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.055, 0, 0);
  tempGeometry[tempGeometry.length - 1].scale(1.5, 1.5, 1.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(0.5);
  tempGeometry[tempGeometry.length - 1].translate(-0.07, 0.14, 0);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.1);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI * 0.8);
  cellObject[5].moleSkin = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0xFF9E80 }));
  clearTempGeometries();
  cellObject[5].mole.add(cellObject[5].moleBody, cellObject[5].eyes, cellObject[5].highlights, cellObject[5].moleSkin);
  cellObject[5].add(cellObject[5].hole, cellObject[5].mole);
  cellObject[5].hole.scale.set(0, 0, 0);
  cellObject[5].mole.scale.set(0, 0, 0);
  cellObject[5].rotation.x = 0.2;
  cellObject[5].position.y = -0.11;
  cellObject[5].container.add(cellObject[5]);
  
  cellObject[6] = new THREE.Object3D();
  cellObject[6].container = new THREE.Object3D();
  
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(0.07, 0.065, 0.2, 16, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, -0.02, 0);
  cellObject[6].body = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0xFF80AB }));
  clearTempGeometries();

  cellObject[6].head = new THREE.Mesh(new THREE.ConeGeometry(0.12, 0.13, 30, 1, true), new THREE.MeshLambertMaterial({ color: 0xFF9E80 }));
  cellObject[6].head.geometry.translate(0, 0.11, 0);
  
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.065, 16);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  
  tempGeometry[tempGeometry.length - 1].translate(0, -0.12, 0);
  
  

  cellObject[6].back = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0xFFEA00 }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(0.12, 30);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  
  tempGeometry[tempGeometry.length - 1].translate(0, 0.045, 0);

  cellObject[6].headBack = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0xCE93D8 }));
  clearTempGeometries();

  
  cellObject[6].fuse = new THREE.Mesh(new THREE.TorusGeometry(0.1, 0.008, 4, 5, 0.8), new THREE.MeshLambertMaterial({ color: 0x8D6E63 }));
  cellObject[6].fuse.geometry.rotateX(Math.PI);
  cellObject[6].fuse.geometry.translate(-0.1, -0.12, 0);
  
  
  let shape = new THREE.Shape();
  shape.moveTo(0.03, 0);
  for (let j = 0; j < 7; j++) {
  	shape.lineTo(0.03 * Math.cos(Math.PI / 3.5 * j), 0.03 * Math.sin(Math.PI / 3.5 * j));
  	shape.lineTo(0.07 * Math.cos(Math.PI / 7 + Math.PI / 3.5 * j), 0.07 * Math.sin(Math.PI / 7 + Math.PI / 3.5 * j));
  }
  cellObject[6].sparkle = new THREE.Mesh(new THREE.ShapeGeometry(shape, 1), new THREE.MeshBasicMaterial({ color: 0xFFFF8D }));

  cellObject[6].sparkle.position.set(-0.04, -0.2, 0.008);
  cellObject[6].sparkle.visible = false;
  cellObject[6].add(cellObject[6].body, cellObject[6].head, cellObject[6].fuse, cellObject[6].sparkle, cellObject[6].back, cellObject[6].headBack);
  cellObject[6].scale.set(0.9, 0.9, 0.9);
  cellObject[6].container.add(cellObject[6]);
  cellObject[6].container.rotation.set(-0.2, -0.1, -Math.PI * 0.15 - 0.1);
  
  
  cellObject[6].container.position.set(0, 0, 1)
  
  //cellObject[6].container.rotation.set(0.2, 0, -)
  	
//cellObject[5].mole.rotation.set(-0.05, -0.1, -0.1);
 //cellsContainer.add(cellObject[6].container);
 
 //
 
	//gsap.to(mainScene.rotation, { duration: 10, y: Math.PI * 2, ease: "none", repeat: -1 });

/*	cellObject[5].mole.rotation.set(-0.05, -0.1, -0.1);
	gsap.to(cellObject[5].mole.rotation, { duration: "random(1.5, 3)", x: 0.05, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
  gsap.to(cellObject[5].mole.rotation, { duration: "random(1.5, 3)", y: 0.1, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
  gsap.to(cellObject[5].mole.rotation, { duration: "random(1.5, 3)", z: 0.1, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });*/



	cellsContainer.clickableAreaGeometry = new THREE.PlaneGeometry(0.34, 0.34);
  
	shape = null;
	shape = new THREE.Shape();
	shape.absarc(0.11, 0.11, 0.05, 0, Math.PI * 0.5);
	shape.absarc(-0.11, 0.11, 0.05, Math.PI * 0.5, Math.PI);
	shape.absarc(-0.11, -0.11, 0.05, Math.PI, Math.PI * 1.5);
	shape.absarc(0.11, -0.11, 0.05, Math.PI * 1.5, Math.PI * 2);
	cellsContainer.cellBodyGeometry = new THREE.ShapeGeometry(shape, 3);

  const soilMaterial = [
    new THREE.MeshBasicMaterial({ color: 0xFF9E80, transparent: true, opacity: 0.7 }),
    new THREE.MeshBasicMaterial({ color: 0xBCAAA4, transparent: true, opacity: 0.7 }),
  ];
	
	for (let i = 0; i < 49; i++) {
		cell[i] = new THREE.Object3D();
		
	  cell[i].body = new THREE.Mesh(cellsContainer.cellBodyGeometry, new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0.2 }));
    gsap.to(cell[i].body.material, { duration: "random(1, 2)", opacity: 0.5, ease: "power1.inOut", repeat: -1, yoyo: true });

		cell[i].clickableArea = new THREE.Mesh(cellsContainer.clickableAreaGeometry, transparentMaterial);

		cell[i].add(cell[i].body, cell[i].clickableArea);
		cell[i].position.set(-1.02 + 0.34 * (i % 7), 1.02 - 0.34 * Math.floor(i / 7), -1);
		cell[i].content = -1;
		cell[i].tempMark = 0;
		cellList.push(i);
		cellsContainer.add(cell[i]);
		
		cell[i].sparkle = [];
		cell[i].ground = [];
		
		
		
		
    for (let j = 0; j < 8; j++) {
    	cell[i].sparkle[j] = new THREE.Mesh(sparkleGeometry[Math.round(Math.random())].clone(), transparentMaterial);
  	  const random = 0.6 + Math.random() * 0.6;
  	  cell[i].sparkle[j].geometry.scale(random, random, 1);
  	  cell[i].sparkle[j].scale.set(0, 0, 1);
   	  cell[i].sparkle[j].visible = false;
   	  
   	  cell[i].ground[j] = new THREE.Mesh(sparkleGeometry[0].clone(), soilMaterial[Math.round(Math.random())]);
   	  
   	  cell[i].ground[j].geometry.scale(random, random, 1);
   	  cell[i].ground[j].scale.set(0, 0, 1);
   	  cell[i].ground[j].visible = false;
   	  
  	  cell[i].add(cell[i].sparkle[j], cell[i].ground[j]);
    }

		
	}
	
	
	cellsContainer.position.set(0, 0, -200);
	
	
	
	/*cellsContainer.cellSelection = new THREE.Object3D();
	cellsContainer.cellSelection.part = [];
	cellsContainer.cellSelection.color = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 });
	for (let i = 0; i < 12; i++) {
		cellsContainer.cellSelection.part[i] = new THREE.Mesh(new THREE.PlaneGeometry(0.06, 0.02), cellsContainer.cellSelection.color);
		cellsContainer.cellSelection.part[i].geometry.translate(0.03, 0, 0);
		cellsContainer.cellSelection.part[i].position.set(0.2 * Math.cos(Math.PI / 6 * i), 0.2 * Math.sin(Math.PI / 6 * i), 0);
		cellsContainer.cellSelection.part[i].rotation.z = Math.PI / 6 * i;
		gsap.to(cellsContainer.cellSelection.part[i].scale, { duration: "random(0.3, 0.5)", x: 3, ease: "power1.inOut", repeat: -1, yoyo: true });

		cellsContainer.cellSelection.add(cellsContainer.cellSelection.part[i]);
	}
	gsap.to(cellsContainer.cellSelection.rotation, { duration: 4, z: Math.PI * -2, ease: "none", repeat: -1 });
  cellsContainer.cellSelection.scale.set(0, 0, 1);*/
  
  cellsContainer.cellSelection = new THREE.Mesh(new THREE.RingGeometry(0.28, 0.32, 48, 1), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
	
	
	
	
	cellsContainer.add(cellsContainer.cellSelection);
	
	cellsContainer.scale.set(140, 140, 140);
	
  moveCounter.text = new THREE.Mesh(new THREE.ExtrudeGeometry(Bagel.generateShapes(`${moveCount}`, 3), { steps: 1, depth: 0.04, curveSegments: 5, bevelEnabled: true, bevelSize: 0.0065, bevelThickness: 0.0065, bevelOffset: -0.0065, bevelSegments: 1 }), new THREE.MeshLambertMaterial({ color: 0xaaaaaa }));
  moveCounter.add(moveCounter.text);
  moveCounter.position.set(0, 1.2, -4);
	
	mainScene.add(cellsContainer);
	
	document.body.addEventListener('touchstart', onDocumentTouchStart, false);
  document.body.addEventListener('touchend', onDocumentTouchEnd, false);
  document.body.addEventListener('touchmove', onDocumentTouchMove, false); 
  updateField()
}



function updateField() {
	
	cell[24].content = 0;
  for (let i = 0; i < 49; i++) {
  	cell[i].pic = i;
  	if (cell[i].content == -1) {
		  cell[i].content = Math.ceil(Math.random() * 3);
	   	for (let j = 0; j < 3; j++) {
		    if (checkMatches([i], false, false)) {
	  	  	cell[i].content = (cell[i].content + 1) % 3 + 1;
  		  } else {
		      break;
	  	  }
	   	}
  	}
  	cellImg[cell[i].pic] = cellObject[cell[i].content].container.clone();
  	
  	
  	cellImg[cell[i].pic].position.set(cell[i].position.x, cell[i].position.y, -0.2);
  	
  	if (i != 24) {
  		cellImg[cell[i].pic].tween_1 = gsap.to(cellImg[cell[i].pic].rotation, { duration: "random(1.5, 3)", x: 0.2, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
      cellImg[cell[i].pic].tween_2 = gsap.to(cellImg[cell[i].pic].rotation, { duration: "random(1.5, 3)", y: 0.2, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
      cellImg[cell[i].pic].tween_3 = gsap.to(cellImg[cell[i].pic].rotation, { duration: "random(1.5, 3)", z: 0.2, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });

  	}
  	
  	cellsContainer.add(cellImg[cell[i].pic]);
  	
	}
	cellImg[24].position.z = 0;
	gsap.to(cellImg[24].children[0].children[0].rotation, { duration: "random(1, 3)", x: "random(-0.1, 0.1)", ease: "power2.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
  gsap.to(cellImg[24].children[0].children[0].rotation, { duration: "random(1, 3)", y: "random(-0.2, 0.2)", ease: "power2.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
  gsap.to(cellImg[24].children[0].children[0].rotation, { duration: "random(1, 3)", z: "random(-0.1, 0.1)", ease: "power2.inOut", repeat: -1, repeatRefresh: true, yoyo: true });


}




function checkMatches(cellsToCheck, fullCheck, playerMove) {
	let matchedCells = [];
	let matchedH = [];
	let matchedV = [];
	function getArraysToCheck(chosenCell) {
		matchedH = [];
		matchedV = [];
    for (let j = 1; j < 7; j++) {
	    if (chosenCell - j >= 0 && cell[chosenCell - j].tempMark == 0 && cell[chosenCell - j].position.y == cell[chosenCell].position.y && cell[chosenCell - j].content == cell[chosenCell].content) {
  	    matchedH.push(chosenCell - j);
	    } else {
	    	break;
	    }
    }
    for (let j = 1; j < 7; j++) {
  	  if (chosenCell + j < 49 && cell[chosenCell + j].tempMark == 0 && cell[chosenCell + j].position.y == cell[chosenCell].position.y && cell[chosenCell + j].content == cell[chosenCell].content) {
  		  matchedH.push(chosenCell + j);
  	  } else {
  		 	break;
  		}
   	}
    for (let j = 7; j < 43; j += 7) {
  	  if (chosenCell - j >= 0 && cell[chosenCell - j].tempMark == 0 && cell[chosenCell - j].position.x == cell[chosenCell].position.x && cell[chosenCell - j].content == cell[chosenCell].content) {
  		 	matchedV.push(chosenCell - j);
  	  } else {
  		 	break;
  	 	}
  	}
   	for (let j = 7; j < 43; j += 7) {
  	  if (chosenCell + j < 49 && cell[chosenCell + j].tempMark == 0 && cell[chosenCell + j].position.x == cell[chosenCell].position.x && cell[chosenCell + j].content == cell[chosenCell].content) {
  		  matchedV.push(chosenCell + j);
  	  } else {
  	  	break;
  		}
   	}
	}
  if (fullCheck) {
  	for (let i = 0; i < cellsToCheck.length; i++) {
  		if (cell[cellsToCheck[i]].tempMark == 0) {
  			getArraysToCheck(cellsToCheck[i]);
  			if ((matchedH.length > 3 || matchedV.length > 3) && matchedH.length > 1 && matchedV.length > 1) {
          matchedCells.push([4, cellsToCheck[i]]);
          matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat(matchedH, matchedV);
  			  for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
  			    cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
  			  }
  			}
  		}
    }
    for (let i = 0; i < cellsToCheck.length; i++) {
    	if (cell[cellsToCheck[i]].tempMark == 0) {
    		getArraysToCheck(cellsToCheck[i]);
    	  if (matchedH.length > 3) {
    	  	matchedCells.push([4]);
    	  	if (playerMove) {
    	  		matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat([cellsToCheck[i]], matchedH);
     	  	} else {
    	  		matchedH.push(cellsToCheck[i]);
    	  		matchedH.sort((a, b) => a - b);
    	  		matchedCells[matchedCells.length - 1].push(matchedH[Math.ceil(matchedH.length / 2) - 1]);
    	  		for (let j = 0; j < matchedH.length; j++) {
              if (j != Math.ceil(matchedH.length / 2) - 1) matchedCells[matchedCells.length - 1].push(matchedH[j]);
    	  		}
    	  	}
    	  	for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
    	  		cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
    	  	}
    	  }
    	  if (matchedV.length > 3 && cell[cellsToCheck[i]].tempMark == 0) {
    	  	matchedCells.push([4]);
    	  	if (playerMove) {
    	  		matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat([cellsToCheck[i]], matchedV);
    	  	} else {
    	  		matchedV.push(cellsToCheck[i]);
    	  		matchedV.sort((a, b) => a - b);
    	  		matchedCells[matchedCells.length - 1].push(matchedV[Math.ceil(matchedV.length / 2) - 1]);
    	  		for (let j = 0; j < matchedV.length; j++) {
    	  			if (j != Math.ceil(matchedV.length / 2) - 1) matchedCells[matchedCells.length - 1].push(matchedV[j]);
    	  		}
    	  	}
    	  	for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
    	  		cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
    	  	}
    	  }
    	}
    }
    for (let i = 0; i < cellsToCheck.length; i++) {
    	if (cell[cellsToCheck[i]].tempMark == 0) {
    		getArraysToCheck(cellsToCheck[i]);
    		if (matchedH.length > 1 && matchedV.length > 1) {
    			matchedCells.push([3, cellsToCheck[i]]);
    			matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat(matchedH, matchedV);
    			for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
    				cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
    			}
    		}
    	}
    }
    for (let i = 0; i < cellsToCheck.length; i++) {
    	if (cell[cellsToCheck[i]].tempMark == 0) {
    		getArraysToCheck(cellsToCheck[i]);
    		if (matchedH.length > 2) {
    			matchedCells.push([2]);
    			if (playerMove) {
    				matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat([cellsToCheck[i]], matchedH);
    			} else {
    				matchedH.push(cellsToCheck[i]);
    				matchedH.sort((a, b) => a - b);
    				matchedCells[matchedCells.length - 1].push(matchedH[Math.ceil(matchedH.length / 2) - 1]);
    				for (let j = 0; j < matchedH.length; j++) {
    					if (j != Math.ceil(matchedH.length / 2) - 1) matchedCells[matchedCells.length - 1].push(matchedH[j]);
    				}
    			}
    			for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
    				cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
    			}
    		}
    		if (matchedV.length > 2 && cell[cellsToCheck[i]].tempMark == 0) {
    			matchedCells.push([2]);
    			if (playerMove) {
    				matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat([cellsToCheck[i]], matchedV);
    			} else {
    				matchedV.push(cellsToCheck[i]);
    				matchedV.sort((a, b) => a - b);
    				matchedCells[matchedCells.length - 1].push(matchedV[Math.ceil(matchedV.length / 2) - 1]);
    				for (let j = 0; j < matchedV.length; j++) {
    					if (j != Math.ceil(matchedV.length / 2) - 1) matchedCells[matchedCells.length - 1].push(matchedV[j]);
    				}
    			}
    			for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
    				cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
    			}
    		}
    	}
    }
	}
	for (let i = 0; i < cellsToCheck.length; i++) {
  	if (cell[cellsToCheck[i]].tempMark == 0) {
	  	if (cellsToCheck[i] % 7 > 0 && cell[cellsToCheck[i] - 1].tempMark == 0 && cell[cellsToCheck[i] - 1].content == cell[cellsToCheck[i]].content) {
	  		if (cellsToCheck[i] < 42 && cell[cellsToCheck[i] + 6].tempMark == 0 && cell[cellsToCheck[i] + 7].tempMark == 0 && cell[cellsToCheck[i] + 6].content == cell[cellsToCheck[i]].content && cell[cellsToCheck[i] + 7].content == cell[cellsToCheck[i]].content) {
	  			matchedCells.push([1, cellsToCheck[i], cellsToCheck[i] - 1, cellsToCheck[i] + 6, cellsToCheck[i] + 7]);
	  			for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
	  				cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
	  			}
	  			addCells(matchedCells.length - 1);
	  			continue;
	  		}
	  		if (cellsToCheck[i] > 6 && cell[cellsToCheck[i] - 7].tempMark == 0 && cell[cellsToCheck[i] - 8].tempMark == 0 && cell[cellsToCheck[i] - 7].content == cell[cellsToCheck[i]].content && cell[cellsToCheck[i] - 8].content == cell[cellsToCheck[i]].content) {
	  			matchedCells.push([1, cellsToCheck[i], cellsToCheck[i] - 1, cellsToCheck[i] - 7, cellsToCheck[i] - 8]);
	  			for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
	  				cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
	  			}
	  			addCells(matchedCells.length - 1);
	  			continue;
	  		}
	  	}
	  	if (cellsToCheck[i] % 7 < 6 && cell[cellsToCheck[i] + 1].tempMark == 0 && cell[cellsToCheck[i] + 1].content == cell[cellsToCheck[i]].content) {
	  		if (cellsToCheck[i] < 42 && cell[cellsToCheck[i] + 7].tempMark == 0 && cell[cellsToCheck[i] + 8].tempMark == 0 && cell[cellsToCheck[i] + 7].content == cell[cellsToCheck[i]].content && cell[cellsToCheck[i] + 8].content == cell[cellsToCheck[i]].content) {
	  			matchedCells.push([1, cellsToCheck[i], cellsToCheck[i] + 1, cellsToCheck[i] + 7, cellsToCheck[i] + 8]);
	  			for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
	  				cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
	  			}
	  			addCells(matchedCells.length - 1);
	  			continue;
	  		}
	  		if (cellsToCheck[i] > 6 && cell[cellsToCheck[i] - 6].tempMark == 0 && cell[cellsToCheck[i] - 7].tempMark == 0 && cell[cellsToCheck[i] - 6].content == cell[cellsToCheck[i]].content && cell[cellsToCheck[i] - 7].content == cell[cellsToCheck[i]].content) {
	  			matchedCells.push([1, cellsToCheck[i], cellsToCheck[i] + 1, cellsToCheck[i] - 6, cellsToCheck[i] - 7]);
	        for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
	        	cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
	        }
	  		  addCells(matchedCells.length - 1);
	  		}
	  	}
  	}
	}
	for (let i = 0; i < cellsToCheck.length; i++) {
		if (cell[cellsToCheck[i]].tempMark == 0) {
			getArraysToCheck(cellsToCheck[i]);
			if (matchedH.length == 2) {
				matchedCells.push([0]);
				if (playerMove) {
					matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat([cellsToCheck[i]], matchedH);
				} else {
					matchedH.push(cellsToCheck[i]);
					matchedH.sort((a, b) => a - b);
					matchedCells[matchedCells.length - 1].push(matchedH[Math.ceil(matchedH.length / 2) - 1]);
					for (let j = 0; j < matchedH.length; j++) {
						if (j != Math.ceil(matchedH.length / 2) - 1) matchedCells[matchedCells.length - 1].push(matchedH[j]);
					}
				}
				for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
					cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
				}
			}
			if (matchedV.length == 2 && cell[cellsToCheck[i]].tempMark == 0) {
				matchedCells.push([0]);
				if (playerMove) {
					matchedCells[matchedCells.length - 1] = matchedCells[matchedCells.length - 1].concat([cellsToCheck[i]], matchedV);
				} else {
					matchedV.push(cellsToCheck[i]);
					matchedV.sort((a, b) => a - b);
					matchedCells[matchedCells.length - 1].push(matchedV[Math.ceil(matchedV.length / 2) - 1]);
					for (let j = 0; j < matchedV.length; j++) {
						if (j != Math.ceil(matchedV.length / 2) - 1) matchedCells[matchedCells.length - 1].push(matchedV[j]);
					}
				}
				for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
					cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
				}
			}
		}
	}
	function addCells(mergedCells) {
		for (let i = 1; i < 5; i++) {
			if (matchedCells[mergedCells][i] > 6 && cell[matchedCells[mergedCells][i] - 7].tempMark == 0 && cell[matchedCells[mergedCells][i] - 7].content == cell[matchedCells[mergedCells][i]].content) matchedCells[mergedCells].push(matchedCells[mergedCells][i] - 7);
		  if (matchedCells[mergedCells][i] < 42 && cell[matchedCells[mergedCells][i] + 7].tempMark == 0 && cell[matchedCells[mergedCells][i] + 7].content == cell[matchedCells[mergedCells][i]].content) matchedCells[mergedCells].push(matchedCells[mergedCells][i] + 7);
      if (matchedCells[mergedCells][i] % 7 > 0 && cell[matchedCells[mergedCells][i] - 1].tempMark == 0 && cell[matchedCells[mergedCells][i] - 1].content == cell[matchedCells[mergedCells][i]].content) matchedCells[mergedCells].push(matchedCells[mergedCells][i] - 1);
      if (matchedCells[mergedCells][i] % 7 < 6 && cell[matchedCells[mergedCells][i] + 1].tempMark == 0 && cell[matchedCells[mergedCells][i] + 1].content == cell[matchedCells[mergedCells][i]].content) matchedCells[mergedCells].push(matchedCells[mergedCells][i] + 1);
		}
		for (let k = 1; k < matchedCells[matchedCells.length - 1].length; k++) {
			cell[matchedCells[matchedCells.length - 1][k]].tempMark = 1;
		}
	}
	cell.forEach(function(item) { item.tempMark = 0 });
	if (matchedCells.length == 0) {
		matchedCells = false;
	}
	return matchedCells;
}







function moveFarmer(dir) {
	if (dir == 8) {
		onSwipe = false;
		if (farmerCell > 6) {
			gsap.to(cellImg[cell[farmerCell].pic].position, { duration: 0.3, y: cellImg[cell[farmerCell].pic].position.y + 0.34, ease: "back.inOut" });
      gsap.to(cellImg[cell[farmerCell - 7].pic].position, { duration: 0.3, y: cellImg[cell[farmerCell - 7].pic].position.y - 0.34, ease: "back.inOut", onComplete: function() {
      	cell[farmerCell].content = cell[farmerCell - 7].content;
      	cell[farmerCell - 7].content = 0;
      	cell[farmerCell].pic = cell[farmerCell - 7].pic;
      	cell[farmerCell - 7].pic = 24;
      	farmerCell -= 7;
      	checkMove(checkMatches([farmerCell + 7], true, true));
      } });
		} else {
			gsap.to(cellImg[cell[farmerCell].pic].position, { duration: 0.1, y: cellImg[cell[farmerCell].pic].position.y + 0.1, ease: "power1.out", repeat: 1, yoyo: true, onComplete: function() {
				onSwipe = true;
			} });
		}
	} else if (dir == 16) {
		onSwipe = false;
		if (farmerCell < 42) {
			gsap.to(cellImg[cell[farmerCell].pic].position, { duration: 0.3, y: cellImg[cell[farmerCell].pic].position.y - 0.34, ease: "back.inOut" });
      gsap.to(cellImg[cell[farmerCell + 7].pic].position, { duration: 0.3, y: cellImg[cell[farmerCell + 7].pic].position.y + 0.34, ease: "back.inOut", onComplete: function() {
      	cell[farmerCell].content = cell[farmerCell + 7].content;
      	cell[farmerCell + 7].content = 0;
      	cell[farmerCell].pic = cell[farmerCell + 7].pic;
      	cell[farmerCell + 7].pic = 24;
      	farmerCell += 7;
      	checkMove(checkMatches([farmerCell - 7], true, true));
      } });
		} else {
			gsap.to(cellImg[cell[farmerCell].pic].position, { duration: 0.1, y: cellImg[cell[farmerCell].pic].position.y - 0.1, ease: "power1.out", repeat: 1, yoyo: true, onComplete: function() {
				onSwipe = true;
			} });
		}
	} else if (dir == 2) {
		onSwipe = false;
		if (farmerCell % 7 > 0) {
			gsap.to(cellImg[cell[farmerCell].pic].position, { duration: 0.3, x: cellImg[cell[farmerCell].pic].position.x - 0.34, ease: "back.inOut" });
      gsap.to(cellImg[cell[farmerCell - 1].pic].position, { duration: 0.3, x: cellImg[cell[farmerCell - 1].pic].position.x + 0.34, ease: "back.inOut", onComplete: function() {
      	cell[farmerCell].content = cell[farmerCell - 1].content;
      	cell[farmerCell - 1].content = 0;
      	cell[farmerCell].pic = cell[farmerCell - 1].pic;
      	cell[farmerCell - 1].pic = 24;
      	farmerCell--;
      	checkMove(checkMatches([farmerCell + 1], true, true));
      } });
		} else {
			gsap.to(cellImg[cell[farmerCell].pic].position, { duration: 0.1, x: cellImg[cell[farmerCell].pic].position.x - 0.1, ease: "power1.out", repeat: 1, yoyo: true, onComplete: function() {
				onSwipe = true;
			} });
		}
	} else if (dir == 4) {
		onSwipe = false;
		if (farmerCell % 7 < 6) {
			gsap.to(cellImg[cell[farmerCell].pic].position, { duration: 0.3, x: cellImg[cell[farmerCell].pic].position.x + 0.34, ease: "back.inOut" });
      gsap.to(cellImg[cell[farmerCell + 1].pic].position, { duration: 0.3, x: cellImg[cell[farmerCell + 1].pic].position.x - 0.34, ease: "back.inOut", onComplete: function() {
      	cell[farmerCell].content = cell[farmerCell + 1].content;
      	cell[farmerCell + 1].content = 0;
      	cell[farmerCell].pic = cell[farmerCell + 1].pic;
      	cell[farmerCell + 1].pic = 24;
      	farmerCell++;
      	checkMove(checkMatches([farmerCell - 1], true, true));
      } });
		} else {
			gsap.to(cellImg[cell[farmerCell].pic].position, { duration: 0.1, x: cellImg[cell[farmerCell].pic].position.x + 0.1, ease: "power1.out", repeat: 1, yoyo: true, onComplete: function() {
				onSwipe = true;
			} });
		}
	}
}

function checkMove(match) {
	
	if (match) {
		goMergeCells(match, true);
	} else {
		addEnemy();
		
	}
}

function goMergeCells(matchedCells, playerMove) {
//	alert('ok')
	console.log(matchedCells)
	let cellCount = 0;
	//alert("go")
	for (let i = 48; i >= 0; i--) {
		//console.log(cell[i].content)
	}
	for (let i = 0; i < matchedCells.length; i++) {
		if (cell[matchedCells[i][1]].content != 0) {
			moveCount += matchedCells[i][0] + 1;
		} else if (playerMove) {
			moveCount -= (matchedCells[i].length - 1);
		}
		moveCounter.text.geometry.dispose();
    moveCounter.text.geometry = new THREE.ExtrudeGeometry(Bagel.generateShapes(`${moveCount}`, 30), { steps: 1, depth: 0.04, curveSegments: 5, bevelEnabled: true, bevelSize: 0.0065, bevelThickness: 0.0065, bevelOffset: -0.0065, bevelSegments: 1 })

    if (matchedCells[i][0] == 1) cell[matchedCells[i][1]].tempMark = 1;
		

  	for (let j = 1; j < matchedCells[i].length; j++) {
  		//console.log("op")
  		//console.log(cell[matchedCells[i][j]].content)
		  const movedCell = cell[matchedCells[i][j]];
		  cellCount++;
		  //console.log(matchedCells[i][j])
		  //console.log(movedCell.content)
		  
		  //if (j == 1) {
		
		  
		 
		 
		 // }
		  gsap.to(movedCell.body.material.color, { duration: 0.5, r: cellColor[movedCell.content].r, g: cellColor[movedCell.content].g, b: cellColor[movedCell.content].b, ease: "back.out", repeat: 1, yoyo: true });
      
      
      const random = 0.3 + Math.random() * 0.2;
      if (playerMove || movedCell.content != 5) {
        for (let k = 0; k < 8; k++) {
          movedCell.sparkle[k].position.set(0, 0, 1.5);
          movedCell.sparkle[k].material = sparkleMaterial[(movedCell.content) * Math.round(Math.random())];
        	movedCell.sparkle[k].visible = true;
        	const sparkleAngle = Math.random() * Math.PI * 2;
        	const sparkleTime = 0.2 + Math.random() * 0.5;
        	const sparkleDist = 0.1 + Math.random() * 0.3;
      	  gsap.to(movedCell.sparkle[k].scale, { duration: sparkleTime * 0.6, x: 0.4, y: 0.4, ease: "power1.out", repeat: 1, yoyo: true, delay: random * 0.3, onComplete: function() { movedCell.sparkle[k].visible = false } });
      	  gsap.to(movedCell.sparkle[k].position, { duration: sparkleTime, x: sparkleDist * Math.cos(sparkleAngle), y: sparkleDist * Math.sin(sparkleAngle), ease: "power1.out", delay: random * 0.3 });
        }
      }
      
      if (movedCell.content != 5) {
		    gsap.to(cellImg[movedCell.pic].children[0].scale, { duration: random, x: cellImg[movedCell.pic].children[0].scale.x * 1.15, y: cellImg[movedCell.pic].children[0].scale.y * 1.15, z: cellImg[movedCell.pic].children[0].scale.z * 1.15, ease: "power1.out" });
		    cellImg[movedCell.pic].children[0].position.z += 0.05 + Math.random() * 0.05;
		    gsap.to(cellImg[movedCell.pic].children[0].position, { duration: random, x: 1, y: 1.6, ease: "back.in", delay: 0.5 });
		    gsap.to(cellImg[movedCell.pic].children[0].scale, { duration: random, x: 0, y: 0, z: 0, ease: "power1.in", delay: 0.5, onComplete: function() {
		      disposeObject(cellImg[movedCell.pic]);
		      if (movedCell.tempMark != 1) {
		        movedCell.content = -1;
		        mergeEnd();
		      } else {
		      	
		      	movedCell.content = -1;
		      	mergeEnd();
		      	/*movedCell.content = matchedCells[i][0] + 5;
		      	
		      	cellImg[movedCell.pic] = cellObject[movedCell.content].container.clone();
		      	cellImg[movedCell.pic].position.set(movedCell.position.x, movedCell.position.y, -0.2);
		      	cellImg[movedCell.pic].tween_1 = gsap.to(cellImg[movedCell.pic].rotation, { duration: "random(1.5, 3)", x: 0.2, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
		      	cellImg[movedCell.pic].tween_2 = gsap.to(cellImg[movedCell.pic].rotation, { duration: "random(1.5, 3)", y: 0.1, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
		      	cellImg[movedCell.pic].tween_3 = gsap.to(cellImg[movedCell.pic].rotation, { duration: "random(1.5, 3)", z: cellImg[movedCell.pic].rotation.z + 0.2, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
		      	cellImg[movedCell.pic].scale.set(0, 0, 0);
		      	cellsContainer.add(cellImg[movedCell.pic]);
		      	gsap.to(cellImg[movedCell.pic].scale, { duration: 0.3, x: 1, y: 1, z: 1, ease: "back.out", onComplete: function() {
		      		movedCell.tempMark = 0;
		      		mergeEnd();
		      	} });*/
            
		      }
		    } });
      } else {
      	gsap.to(cellImg[movedCell.pic].children[0].scale, { duration: random * 0.6, x: cellImg[movedCell.pic].children[0].scale.x * 1.05, y: cellImg[movedCell.pic].children[0].scale.y * 1.05, z: cellImg[movedCell.pic].children[0].scale.z * 1.15, ease: "power1.out" });
        gsap.to(cellImg[movedCell.pic].children[0].children[1].scale, { duration: random * 0.8, x: 0, y: 0, z: 0, ease: "back.in", delay: 0.3, onComplete: function() {
        	
        		for (let k = 0; k < 8; k++) {
        			movedCell.ground[k].position.set(0, -0.15, 0.2);
        			movedCell.ground[k].visible = true;
        			const sparkleTime = 0.1 + Math.random() * 0.25;
        			const sparkleDist = 0.15 + Math.random() * 0.1;
        			gsap.to(movedCell.ground[k].scale, { duration: sparkleTime * 0.9, x: 0.3, y: 0.3, ease: "power1.out", repeat: 1, yoyo: true });
        			gsap.to(movedCell.ground[k].position, { duration: sparkleTime, y: -0.15 + sparkleDist, ease: "power1.out", repeat: 1, yoyo: true });
        			gsap.to(movedCell.ground[k].position, { duration: sparkleTime * 2, x: "random(-0.25, 0.25)", ease: "none", onComplete: function() { movedCell.ground[k].visible = false } });
        		}
        	
        } });
      	gsap.to(cellImg[movedCell.pic].children[0].children[0].scale, { duration: random, x: 0, y: 0, z: 0, ease: "power2.in", delay: 0.3 + random * 0.3, onComplete: function() {
      		movedCell.content = -1;
      		disposeObject(cellImg[movedCell.pic]);
       		mergeEnd();
      	} });
      }
    }
	}
	function mergeEnd() {
		cellCount--;
		if (cellCount == 0) {
			
			goFillEmptyCells();
			
		}
	}
}

function disposeObject(object) {
	
	object.tween_1.kill();
	object.tween_1 = null;
	object.tween_2.kill();
	object.tween_2 = null;
	object.tween_3.kill();
	object.tween_3 = null;
	object.parent.remove(object);
	const list = [];
	object.traverse((child) => {
		if (child.isMesh) list.push(child);
	});
	for (let i = 0; i < list.length; i++) {
		list[i].geometry.dispose();
		list[i].material.dispose();
		list[i] = null;
	}
	object = null;
}

function goFillEmptyCells() {
	const newCells = [];
	const candidateCells = [];
	for (let i = 48; i >= 0; i--) {
		if (cell[i].content == -1) {
			newCells.push(i);
			let newCell = i - 7;
			while (cell[i].content == -1) {
				if (newCell < 0) {
					cell[i].content = Math.ceil(Math.random() * 4);
					for (let j = 0; j < 4; j++) {
						if (checkMatches([i], false, false)) {
							cell[i].content = (cell[i].content + 1) % 4 + 1;
						} else {
							break;
						}
					}
					cellImg[cell[i].pic] = cellObject[cell[i].content].container.clone();
					cellImg[cell[i].pic].position.set(cell[i].position.x, 2.38, -0.2);
				  cellImg[cell[i].pic].tween_1 = gsap.to(cellImg[cell[i].pic].rotation, { duration: "random(1.5, 3)", x: 0.2, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
				  cellImg[cell[i].pic].tween_2 = gsap.to(cellImg[cell[i].pic].rotation, { duration: "random(1.5, 3)", y: 0.2, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
					cellImg[cell[i].pic].tween_3 = gsap.to(cellImg[cell[i].pic].rotation, { duration: "random(1.5, 3)", z: 0.2, ease: "power1.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
					cellImg[cell[i].pic].scale.set(0, 0, 0);
					cellsContainer.add(cellImg[cell[i].pic]);
				} else {
					if (cell[newCell].content != -1) {
						const tempPic = cell[i].pic;
						cell[i].pic = cell[newCell].pic;
						cell[newCell].pic = tempPic;
						cell[i].content = cell[newCell].content;
						if (cell[newCell].content > 5 && activePowerUp > -1) activePowerUp = i;

						if (cell[newCell].content == 0) farmerCell = i;
						
						cell[newCell].content = -1;
					} else {
						newCell -= 7;
					}
				}
			}
		}
	}
	for (let i = 0; i < newCells.length; i++) {
		gsap.to(cellImg[cell[newCells[i]].pic].position, { duration: 0.25, y: cell[newCells[i]].position.y, ease: "power2.out", delay: 0.03 * i });
		
		if (cellImg[cell[newCells[i]].pic].scale.x == 0) gsap.to(cellImg[cell[newCells[i]].pic].scale, { duration: 0.25, x: 1, y: 1, z: 1, ease: "back.out", delay: 0.03 * i });
	}
	setTimeout(function() {
		requestAnimationFrame(function() {
			const matchedCells = checkMatches(cellList, true, false);
			if (matchedCells) {
				goMergeCells(matchedCells, false);
			} else {
				addEnemy();
				
			}
		});
	}, 200 + 30 * (newCells.length - 1));
}



function addEnemy() {
	let isFree = true;
	let enemyCell = Math.floor(Math.random() * 49);
  for (let i = 0; i < 49; i++) {
  	isFree = true;
    if (cell[enemyCell].content == 0 || cell[enemyCell].content == 5) {
    	isFree = false;
    } else {
      const tempContent = cell[enemyCell].content;
      cell[enemyCell].content = 5;
      
      if (checkMatches([enemyCell], true, false)) {
      	cell[enemyCell].content = tempContent;
      	isFree = false;
      }
    }
    if (isFree) {
    	gsap.to(cellImg[cell[enemyCell].pic].scale, { duration: 0.3, x: 0, y: 0, z: 0, ease: "back.in" });
      gsap.to(cellImg[cell[enemyCell].pic].position, { duration: 0.3, y: cellImg[cell[enemyCell].pic].position.y - 0.15, ease: "back.in", onComplete: function() {
      	disposeObject(cellImg[cell[enemyCell].pic]);
       	cellImg[cell[enemyCell].pic] = cellObject[cell[enemyCell].content].container.clone();
        cellImg[cell[enemyCell].pic].tween_1 = gsap.to(cellImg[cell[enemyCell].pic].children[0].children[1].rotation, { duration: "random(0.8, 2)", x: "random(-0.05, 0.05)", ease: "power2.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
        cellImg[cell[enemyCell].pic].tween_2 = gsap.to(cellImg[cell[enemyCell].pic].children[0].children[1].rotation, { duration: "random(0.8, 2)", y: "random(-0.3, 0.3)", ease: "power2.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
        cellImg[cell[enemyCell].pic].tween_3 = gsap.to(cellImg[cell[enemyCell].pic].children[0].children[1].rotation, { duration: "random(0.8, 2)", z: "random(-0.1, 0.1)", ease: "power2.inOut", repeat: -1, repeatRefresh: true, yoyo: true });
        cellImg[cell[enemyCell].pic].position.set(cell[enemyCell].position.x, cell[enemyCell].position.y, -0.2);
        cellsContainer.add(cellImg[cell[enemyCell].pic]);
        for (let k = 0; k < 8; k++) {
        	cell[enemyCell].ground[k].position.set(0, -0.15, 1);
         	cell[enemyCell].ground[k].visible = true;
        	const sparkleTime = 0.2 + Math.random() * 0.6;
        	const sparkleDist = 0.3 + Math.random() * 0.2;
        	gsap.to(cell[enemyCell].ground[k].scale, { duration: sparkleTime * 0.9, x: 0.4, y: 0.4, ease: "power1.out", repeat: 1, yoyo: true });
        	gsap.to(cell[enemyCell].ground[k].position, { duration: sparkleTime, y: -0.15 + sparkleDist, ease: "power1.out", repeat: 1, yoyo: true });
          gsap.to(cell[enemyCell].ground[k].position, { duration: sparkleTime * 2, x: "random(-0.3, 0.3)", ease: "none", onComplete: function() { cell[enemyCell].ground[k].visible = false } });
        }
        gsap.to(cellImg[cell[enemyCell].pic].children[0].children[0].scale, { duration: 0.15, x: 1, y: 1, z: 1, ease: "power1.out" });
        gsap.to(cellImg[cell[enemyCell].pic].children[0].children[1].scale, { duration: 0.3, x: 1, y: 1, z: 1, ease: "back.out", onComplete: function() { onSwipe = true } });
      } });
     	break;
    } else {
  	  enemyCell = (enemyCell + 1) % 49;
    }
  }
  if (!isFree) onSwipe = true;
}

function activatePowerUp(chosen) {
	if (activePowerUp < 0) {
		activePowerUp = chosen;
		selectPowerUp();
	} else {
		if (activePowerUp == chosen) {
			goFire();
		} else {
			deselectPowerUp(activePowerUp);
			activePowerUp = chosen;
			selectPowerUp();
		}
	}
  function selectPowerUp() {
		if (cellsContainer.cellSelection.scaleTween !== undefined && cellsContainer.cellSelection.scaleTween !== null) {
			cellsContainer.cellSelection.scaleTween.kill();
			cellsContainer.cellSelection.scaleTween = null;
			cellsContainer.cellSelection.opacityTween.kill();
			cellsContainer.cellSelection.opacityTween = null;
		}
	  cellsContainer.cellSelection.position.set(cell[chosen].position.x, cell[chosen].position.y, 1);
    cellsContainer.cellSelection.scaleTween = gsap.fromTo(cellsContainer.cellSelection.scale, { x: 0.5, y: 0.5 }, { duration: 0.4, x: 2, y: 2, ease: "power1.out", repeat: -1, repeatDelay: 0.6 });
    cellsContainer.cellSelection.opacityTween = gsap.fromTo(cellsContainer.cellSelection.material, { opacity: 0.8 }, { duration: 0.4, opacity: 0, ease: "power1.out", repeat: -1, repeatDelay: 0.6 });
		if (cellImg[cell[chosen].pic].scaleTween !== undefined && cellImg[cell[chosen].pic].scaleTween !== null) {
			cellImg[cell[chosen].pic].scaleTween.kill();
			cellImg[cell[chosen].pic].scaleTween = null;
		}
		cellImg[cell[chosen].pic].scaleTween = gsap.to(cellImg[cell[chosen].pic].children[0].scale, { duration: 0.2, x: 1.05, y: 1.05, z: 1.05, ease: "back.out", onComplete: function() {
			cellImg[cell[chosen].pic].scaleTween.kill();
			cellImg[cell[chosen].pic].scaleTween = null;
			cellImg[cell[chosen].pic].scaleTween = gsap.to(cellImg[cell[chosen].pic].children[0].scale, { duration: 0.5, x: 1, y: 1, z: 1, ease: "power1.inOut", repeat: -1, yoyo: true });
		} });
		if (cell[chosen].content == 6) {
			cellImg[cell[chosen].pic].sparkleTween_1 = gsap.to(cellImg[cell[chosen].pic].children[0].children[3].rotation, { duration: 2, z: Math.PI * 2, ease: "none", repeat: -1 });
      cellImg[cell[chosen].pic].sparkleTween_2 = gsap.to(cellImg[cell[chosen].pic].children[0].children[3].scale, { duration: "random(0.03, 0.07)", x: 0.4, ease: "power1.inOut", repeat: -1, yoyo: true });
      cellImg[cell[chosen].pic].sparkleTween_3 = gsap.to(cellImg[cell[chosen].pic].children[0].children[3].scale, { duration: "random(0.03, 0.07)", y: 0.4, ease: "power1.inOut", repeat: -1, yoyo: true });
			cellImg[cell[chosen].pic].children[0].children[3].visible = true;
		}
	}
	function goFire() {
		activePowerUp = -1;
		
		isCellsFieldActive = false;
		onSwipe = false;
    cellsContainer.cellSelection.scaleTween.kill();
    cellsContainer.cellSelection.scaleTween = null;
    cellsContainer.cellSelection.opacityTween.kill();
    cellsContainer.cellSelection.opacityTween = null;
    cellsContainer.cellSelection.scaleTween = gsap.to(cellsContainer.cellSelection.scale, { duration: 0.2, x: 2, y: 2, ease: "power1.out" });
    cellsContainer.cellSelection.opacityTween = gsap.to(cellsContainer.cellSelection.material, { duration: 0.2, opacity: 0, ease: "power1.out" });
    
    cellImg[cell[chosen].pic].scaleTween.kill();
  	cellImg[cell[chosen].pic].scaleTween = null;
  	cellImg[cell[chosen].pic].tween_1.kill();
  	cellImg[cell[chosen].pic].tween_1 = null;
  	cellImg[cell[chosen].pic].tween_2.kill();
  	cellImg[cell[chosen].pic].tween_2 = null;
  	cellImg[cell[chosen].pic].tween_3.kill();
  	cellImg[cell[chosen].pic].tween_3 = null;
  	
  	
  	
  	if (cell[chosen].content == 6) {
  		const molesArray = [];
  		const harvestArray = [];
  		for (let i = 0; i < 49; i++) {
  			if (cell[i].content == 5) molesArray.push(i);
  			if (cell[i].content > 0 && cell[i].content < 5) harvestArray.push(i);
  		}
  		gsap.to(cellImg[cell[chosen].pic].rotation, { duration: 0.3, x: 0, y: 0, z: 0, ease: "power1.inOut" });

  		gsap.to(cellImg[cell[chosen].pic].children[0].rotation, { duration: 0.3, x: Math.PI * 0.5, y: 0, z: 0, ease: "power1.inOut" });
  	  gsap.to(cellImg[cell[chosen].pic].children[0].scale, { duration: 0.3, x: 0.6, y: 0.6, z: 0.6, ease: "back.in", onComplete: function() { 
		    cellImg[cell[chosen].pic].sparkleTween_1.kill();
		    cellImg[cell[chosen].pic].sparkleTween_1 = null;
		    cellImg[cell[chosen].pic].sparkleTween_2.kill();
		    cellImg[cell[chosen].pic].sparkleTween_2 = null;
		    cellImg[cell[chosen].pic].sparkleTween_3.kill();
		    cellImg[cell[chosen].pic].sparkleTween_3 = null;
		    cellImg[cell[chosen].pic].children[0].children[2].visible = false;
		    cellImg[cell[chosen].pic].children[0].children[3].visible = false;
		    
		    let targetCell;
		    cellImg[cell[chosen].pic].position.z = 1;
		    if (molesArray.length > 0) {
		    	targetCell = molesArray[Math.floor(Math.random() * molesArray.length)];
		    	
		    	cellImg[cell[chosen].pic].rotation.z = new THREE.Vector2(cell[chosen].position.x, cell[chosen].position.y).angleTo(new THREE.Vector2(cell[targetCell].position.x, cell[targetCell].position.y)) - Math.PI * 0.5;
		    	
		    	gsap.to(cellImg[cell[chosen].pic].children[0].rotation, { duration: 4.6, x: -Math.PI * 0.5, ease: "power1.inOut" });
          gsap.to(cellImg[cell[chosen].pic].children[0].scale, { duration: 2.3, x: 1.2, y: 1.2, z: 1.2, ease: "power1.in", onComplete: function() {
          	gsap.to(cellImg[cell[chosen].pic].children[0].scale, { duration: 2.3, x: 0, y: 0, z: 0, ease: "power1.out" });

          } });

		    	gsap.to(cellImg[cell[chosen].pic].position, { duration: 4.6, x: cell[targetCell].position.x, y: cell[targetCell].position.y, ease: "power1.inOut" });

		    }
		    
		    
  	  } });
  	}
  	
	}
}

function deselectPowerUp(chosen) {
	activePowerUp = -1;
	cell[chosen].tempMark = 0;
	isCellsFieldActive = false;
  cellsContainer.cellSelection.scaleTween.kill();
  cellsContainer.cellSelection.scaleTween = null;
  cellsContainer.cellSelection.opacityTween.kill();
  cellsContainer.cellSelection.opacityTween = null;
  cellsContainer.cellSelection.scaleTween = gsap.to(cellsContainer.cellSelection.scale, { duration: 0.2, x: 2, y: 2, ease: "power1.out" });
  cellsContainer.cellSelection.opacityTween = gsap.to(cellsContainer.cellSelection.material, { duration: 0.2, opacity: 0, ease: "power1.out" });
	if (cell[chosen].content == 6) {
		cellImg[cell[chosen].pic].sparkleTween_1.kill();
		cellImg[cell[chosen].pic].sparkleTween_1 = null;
		cellImg[cell[chosen].pic].sparkleTween_2.kill();
		cellImg[cell[chosen].pic].sparkleTween_2 = null;
		cellImg[cell[chosen].pic].sparkleTween_3.kill();
		cellImg[cell[chosen].pic].sparkleTween_3 = null;
		cellImg[cell[chosen].pic].children[0].children[3].visible = false;
		cellImg[cell[chosen].pic].children[0].children[3].scale.set(1, 1, 1);
		cellImg[cell[chosen].pic].children[0].children[3].rotation.z = 0;
	}
	cellImg[cell[chosen].pic].scaleTween.kill();
	cellImg[cell[chosen].pic].scaleTween = null;
	gsap.to(cellImg[cell[chosen].pic].children[0].scale, { duration: 0.1, x: 0.9, y: 0.9, z: 0.9, ease: "power1.out", onComplete: function() { isCellsFieldActive = true } });
}



let hammertime = new Hammer(document.body);
hammertime.get('swipe').set({ enable: true, direction: Hammer.DIRECTION_ALL });
hammertime.on('swipe', (e) => {
	if (onPlay && onSwipe) {
		
		moveFarmer(e.direction);
		if (activePowerUp > -1) {
			deselectPowerUp(activePowerUp);
		}
	}
});


let onTouch = false;
const raycaster = new THREE.Raycaster(), mouse = new THREE.Vector2();
function onDocumentTouchStart(event) {
  event.preventDefault();
  onTouch = true;
  mouse.x = (event.changedTouches[0].clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.changedTouches[0].clientY / window.innerHeight) * 2 + 1;
  raycaster.setFromCamera(mouse, mainCamera);
  if (event.touches.length == 1) checkUserAction("touch");
}
function onDocumentMouseDown(event) {
	event.preventDefault();
	if (!onTouch) {
		mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
		mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
		raycaster.setFromCamera(mouse, mainCamera);
		checkUserAction(raycaster);
	}
}
function onDocumentTouchMove(event) {
  event.preventDefault();
  mouse.x = (event.changedTouches[0].clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.changedTouches[0].clientY / window.innerHeight) * 2 + 1;
  raycaster.setFromCamera(mouse, mainCamera);
  if (event.touches.length == 1) checkUserAction("move");
}
function onDocumentTouchEnd() {
  if (isCellsFieldActive && onSwipe) {
    if (activePowerUp > -1) {
    	if (raycaster.intersectObject(cell[activePowerUp].clickableArea).length > 0) {
    	  if (cell[activePowerUp].tempMark == 1) {
    	  	activatePowerUp(activePowerUp);
    	  } else {
    	  	cell[activePowerUp].tempMark = 1;
    	  }
      } else {
      	deselectPowerUp(activePowerUp);
      }
    }
  }
}
function onDocumentMouseMove(event) {
	
}
function checkUserAction(event) {
	if (isCellsFieldActive) {
		if (event == "touch") {
		  for (let i = 0; i < 49; i++) {
			  if (raycaster.intersectObject(cell[i].clickableArea).length > 0 && cell[i].content > 5 && cell[i].tempMark == 0) {
				  activatePowerUp(i);
		  		break;
		  	}
		  }
		  
		  if (activePowerUp > -1 && raycaster.intersectObject(cell[activePowerUp].clickableArea).length == 0) deselectPowerUp(activePowerUp);
    	

		  
		}
		/*if (event == "move") {
		  for (let i = 1; i < cellsToCheck.length; i++) {
			  if (raycaster.intersectObject(cell[cellsToCheck[i]].clickableArea).length > 0) {
			  	//if (cell[cellsToCheck[i]].content == 0) {
			  		
			  //	} else {
			    	//goMoveCell(cellsToCheck[0], cellsToCheck[i]);
		    		cellsToCheck = [];
			  //	}
				  break;
			  }
		  }
		}*/
	}
}

function clearTempGeometries() { while (tempGeometry.length > 0) { tempGeometry[tempGeometry.length - 1].dispose(); tempGeometry.pop(); } }
function bendGeometry(geometry, axis, angle) { let theta = 0; if (angle !== 0) { const v = geometry.attributes.position.array; for (let i = 0; i < v.length; i += 3) { let x = v[i]; let y = v[i + 1]; let z = v[i + 2]; switch (axis) { case "x": theta = z * angle; break; case "y": theta = x * angle; break; default: theta = x * angle; break; } let sinTheta = Math.sin(theta); let cosTheta = Math.cos(theta); switch (axis) { case "x": v[i] = x; v[i + 1] = (y - 1.0 / angle) * cosTheta + 1.0 / angle; v[i + 2] = -(y - 1.0 / angle) * sinTheta; break; case "y": v[i] = -(z - 1.0 / angle) * sinTheta; v[i + 1] = y; v[i + 2] = (z - 1.0 / angle) * cosTheta + 1.0 / angle; break; default: v[i] = -(y - 1.0 / angle) * sinTheta; v[i + 1] = (y - 1.0 / angle) * cosTheta + 1.0 / angle; v[i + 2] = z; break; } } geometry.attributes.position.needsUpdate = true; } }
const stats = new Stats();
//document.body.appendChild(stats.dom);
const clock = new THREE.Clock();
let delta;
loop();
function loop() {
  if (oldWindow !== document.body.clientWidth / document.body.clientHeight) onWindowResize();
  delta = clock.getDelta() * 0.4;
/*  if (onHold) {
  	delta = 0;
  	onHold = false;
  }*/
  
  mainRenderer.render(mainScene, mainCamera);
  //stats.update();
  requestAnimationFrame(loop);
}
/*visibilityChange();
function visibilityChange() {
  let hidden = 'hidden';
  if (hidden in document) document.addEventListener('visibilitychange', onchange);
  else if ((hidden = 'mozHidden') in document) document.addEventListener('mozvisibilitychange', onchange);
  else if ((hidden = 'webkitHidden') in document) document.addEventListener('webkitvisibilitychange', onchange);
  else if ((hidden = 'msHidden') in document) document.addEventListener('msvisibilitychange', onchange);
  else if ('onfocusin' in document) document.onfocusin = document.onfocusout = onchange;
  else window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;
  function onchange(evt) {
    let v = false;
    let h = true;
    let evtMap = {
      focus: v,
      focusin: v,
      pageshow: v,
      blur: h,
      focusout: h,
      pagehide: h
    };
    evt = evt || window.event;
    let windowHidden = false;
    if (evt.type in evtMap) {
      windowHidden = evtMap[evt.type];
    } else {
      windowHidden = this[hidden];
    }
    if (windowHidden) {
      Howler.mute(true);
      onHold = true;
    } else {
      if (button[8] !== undefined && button[8].onIcon.visible) Howler.mute(false);
    }
  }
  if (document[hidden] !== undefined) {
    onchange({
      type: document[hidden] ? 'blur' : 'focus'
    });
  }
}*/
